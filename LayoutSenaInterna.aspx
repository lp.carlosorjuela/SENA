﻿<%@ Page language="C#"   Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=15.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
	<style type="text/css"> 
	   #pageTitle{display:none!important;}
	   #container{ background-color:#fff !important; margin-bottom:20px; -moz-border-radius:0 0 8px 8px; -webkit-border-radius:0 0 8px 8px; border-radius:0 0 8px 8px;}
	   #container{border:1px #d4d4d4 solid; padding:10px; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;}
	   
	</style>
</asp:Content>

<asp:Content ContentPlaceHolderId="WeatherPart" runat="server">
	<WebPartPages:SPProxyWebPartManager runat="server" id="spproxywebpartmanager"></WebPartPages:SPProxyWebPartManager>	<div class="fullWidth titlePag clear">
		<div class="dkgrey">
				<SharePointWebControls:TextField FieldName="fa564e0f-0c70-4ab9-b863-0177e6ddd247" runat="server"></SharePointWebControls:TextField>
			<span class="orange titlePagBold">
				<SharePointWebControls:TextField FieldName="ff92f929-d18b-46d4-9879-521378c689ef" runat="server"></SharePointWebControls:TextField>
			</span>	
		</div>
	</div>
		
	<div class="fullWidth clear push10">
		<div class="fraseSite">
	    	<SharePointWebControls:TextField FieldName="SeoMetaDescription" runat="server" id="TextField1"></SharePointWebControls:TextField>
		</div>
	</div>  
</asp:Content>

<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">
	<div class="fullWidth clear">
	
		<div class="imgContent">
			<img src="/PublishingImages/foto.jpg" alt="" title=""/> 
			<div class="pie">Pie de foto para creditos: fecha, autor y/o lugar</div>
		</div>
		<div class="buble">
		  <p>Los requerminetos de ls Instructores son permanentes y responden a 
			las necesidades de cada centro de formacion.</p>
		</div>
	</div>
	
	<WebPartPages:WebPartZone id="g_89D39C2F0C7749E184CF5E44E62CC234" runat="server" title="Zona 1">
	</WebPartPages:WebPartZone>
	
	<div class="fullWidth clear">	
		<PublishingWebControls:RichHtmlField FieldName="f55c4d88-1f2e-4ad9-aaa8-819af4ee7ee8" runat="server"></PublishingWebControls:RichHtmlField>
	</div>
</asp:Content>
