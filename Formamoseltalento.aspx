<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Estudia en el SENA</title>

    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta name="author" content="SENA" />
      <meta name="description" content="Participe en la presente oferta educativo, inscripciones abiertas entre el 16 y 25 de Febrero de 2018"/>
      <meta name="keywords" content="Formación, inscripción, SENA, estudia"/>

    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Presidencia -->
   
 

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Fira+Sans:400,500' rel='stylesheet' type='text/css'>
      <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/SiteCollectionImages/favicon.ico" type="image/vnd.microsoft.icon" id="favicon" />

    <!-- GOOGLE ANALYTICS -->
      
    <!-- END GOOGLE ANALYTICS --> 
    <style>
        .container { 
            max-width: 1024px;                
        } 
        html {
   margin: 0px;
   height: 100%;
   width: 100%;
}

body {
   margin: 0px;
   min-height: 100%;
   width: 100%;
}

    </style>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-57872819-1', 'auto');
    ga('send', 'pageview');

</script>
  </head>
<body style="    background: #f6f0e6;
    background-image: url(PortadaFalsa_fondo.png);" >
     
    <div class="container justify-content-center">
        <div class="row">
            <div class="col-md-12" role="main">
                <div class="row justify-content-end" style="margin-top: 5%;">
                    
                    <div id="botonCerrar"class=".col-xs-12" style="
    margin-bottom: 20px;">

                        <a href="http://www.sena.edu.co/es-co/Paginas/default.aspx">   <img src="cerrar_123.png" align="right" class="" alt="Responsive image" style="    padding-right: 35px;"></a>
                    </div>
                </div>
                <div class="row justify-content-center">
                    
                    <div id="imagen" class=".col-xs-12" style="padding-left: 15px; padding-right: 15px;">
                        <a href="http://oferta.senasofiaplus.edu.co/sofia-oferta/buscar-oferta-educativa.html">    <img src="PortadaFalsa_banner.png" class="img-fluid" alt="Responsive image"> </a>
                    </div>
                    
                </div>
                <div class="row justify-content-center" >
                    <div class=".col-xs-12" style="    padding-top: 13px;
    /* display: inline-block; */
    /* vertical-align: middle; */
    width: 310px;
    border-radius: 20px;
    font-weight: bold;
    /* background-color: #b9cc29; */
    /* margin-top: 20px; */
    margin-left: 0px;
    margin-right: 0px;">
                        <a href="http://www.sena.edu.co/es-co/Paginas/default.aspx" style="text-decoration: none;cursor:pointer;
    color: black;"   ><p class="align-middle" style=" color: white;text-align:center;font-family: 'Montserrat', sans-serif;">Continua al portal web del SENA</p></a>
                    </div>

                </div>
            </div>
        </div>
      </div>
    	<footer class="row" style="padding: 14px;">
    	
    	</footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript">
    $('body').click(function(e) {
    
        window.open("http://www.sena.edu.co/es-co/Paginas/default.aspx", "_self");
       
    });
</script>
  </body>
</html>