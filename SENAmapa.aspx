﻿<%@ Page language="C#"   Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=15.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
	<style type="text/css"> 
	   #pageTitle{display:none!important;}
	   #container{ background-color:#fff !important; margin-bottom:20px; -moz-border-radius:0 0 8px 8px; -webkit-border-radius:0 0 8px 8px; border-radius:0 0 8px 8px;}
	   #container{border:1px #d4d4d4 solid; padding:10px; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;}
		
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderId="WeatherPart" runat="server">
	<div class="fullWidth titlePag clear">
		<div class="dkgrey">
				<SharePointWebControls:TextField FieldName="fa564e0f-0c70-4ab9-b863-0177e6ddd247" runat="server"></SharePointWebControls:TextField>
			<span class="orange titlePagBold">
				<SharePointWebControls:TextField FieldName="ff92f929-d18b-46d4-9879-521378c689ef" runat="server"></SharePointWebControls:TextField>
			</span>	
		</div>
	</div>
	
	<div class="fullWidth clear push10">
		<div class="fraseSite">
	    	<SharePointWebControls:TextField FieldName="SeoMetaDescription" runat="server" id="TextField1"></SharePointWebControls:TextField>
		</div>
	</div> 	 
</asp:Content>

<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">
<div id="espacioMapa">
	<div class="container justify-content-center" style="max-width:723px;min-width:347px;">
    <div class="row">
        <div class="col-sm-12 text-center center-block"  style="text-align: justify;">
           <div id="contMap" class="estyleCont" style="background-color: white;/*height: 480px*/;display: block">
               <div id="map" style="height: 400px;width: 100%;">
                
                </div>           
            </div>    
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center center-block"  style="    text-align: justify;">
                 <div id="info" class="infolabel">
                        <div id="infoList" class="infolabeldescip">
                              <div class="row">
                                      <div class="col-sm-6 text-center">
                                           <div id="imgCont">
                                                <img id="imgRegio" src="/es-co/Noticias/NoticiasImg/Comunicado.png" class="imgs" alt="Sede SENA" style="    border: solid #fc7323;"/>
                                            </div>     
                                    </div>
                                    <div class="col-sm-6 text-center center-block">
                                            <div id="infCon" class="contInf">
                                               
                                               <h3 style="margin-top: 0px;" id="nombRe"></h3>
                                            
                                                <p id="descRe" style="text-align: justify;"></p>
                                            </div>
                                    </div>
                                    <div id="btnAtras" class="atrasBtn"> <img  src="/es-co/Noticias/SiteAssets/img/back.png" class="flecaBack" alt="atras" style="" /><div class="atras">
										Atrás</div></div>
                                </div>
                        </div>
                        <div id="RegionalLeyenda">
                          <div class="row">   
                               <div class="col-sm-12" >
                                   <div id="tituloRe" class="titulLeye">
                                       <h2 style="margin-top: 0px;color:white !important;"> <span style="font-weight: lighter;">
										Regional</span> Atlántico</h2>
                                    </div>
                               </div>
                            </div>
                           <div class="row">   
                               <div class="col-sm-12" >
                                       <div class="margTopNo" id="elementos">
                                            <table class="table">
                                                <thead>
                                                      <tr>
                                                        <th style="width: 80%;" >
														CENTRO</th>
                                                        <th style="width: 20%;">
														BLOG</th>
                                                      </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </div>
                        </div>
                </div>
        
        </div>
    </div>
</div>  
</div>
<div class="fullWidth clear">	
		<PublishingWebControls:RichHtmlField FieldName="f55c4d88-1f2e-4ad9-aaa8-819af4ee7ee8" runat="server"></PublishingWebControls:RichHtmlField>
</div>
</asp:Content>
