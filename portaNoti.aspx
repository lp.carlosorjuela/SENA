﻿<%@ Page Inherits="Microsoft.SharePoint.Publishing.TemplateRedirectionPage,Microsoft.SharePoint.Publishing,Version=15.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" %> <%@ Reference VirtualPath="~TemplatePageUrl" %> <%@ Reference VirtualPath="~masterurl/custom.master" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<html xmlns:mso="urn:schemas-microsoft-com:office:office" xmlns:msdt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"><head>
<!--[if gte mso 9]><SharePoint:CTFieldRefs runat=server Prefix="mso:" FieldList="FileLeafRef,Comments,PublishingStartDate,PublishingExpirationDate,PublishingContactEmail,PublishingContactName,PublishingContactPicture,PublishingPageLayout,PublishingVariationGroupID,PublishingVariationRelationshipLinkFieldID,PublishingRollupImage,Audience,PublishingIsFurlPage,PublishingPageImage,PublishingPageContent,SummaryLinks,SummaryLinks2,SeoBrowserTitle,SeoMetaDescription,SeoKeywords,RobotsNoIndex"><xml>
<mso:CustomDocumentProperties>
<mso:ContentType msdt:dt="string">Página principal</mso:ContentType>
<mso:PublishingPageLayout msdt:dt="string">http://172.16.2.128:82/_catalogs/masterpage/aSENAHome.aspx, SENA Home</mso:PublishingPageLayout>
<mso:PublishingPageContent msdt:dt="string">&lt;h2&gt;​Este es su sitio de publicación&lt;/h2&gt;&lt;h3 class=&quot;ms-rteElement-H3&quot;&gt;Estos enlaces le ayudarán a empezar.&lt;/h3&gt;</mso:PublishingPageContent>
<mso:PublishingPageLayoutName msdt:dt="string">aSENAHome.aspx</mso:PublishingPageLayoutName>
<mso:PublishingVariationGroupID msdt:dt="string">fbce4ce6-41fa-4478-b76e-b53c247037f4</mso:PublishingVariationGroupID>
<mso:PublishingVariationRelationshipLinkFieldID msdt:dt="string">http://172.16.2.128:82/Relationships%20List/4_.000, /Relationships List/4_.000</mso:PublishingVariationRelationshipLinkFieldID>
<mso:RequiresRouting msdt:dt="string">False</mso:RequiresRouting>
<mso:ContentTypeId msdt:dt="string">0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF390064DEA0F50FC8C147B0B6EA0636C4A7D40091B64E4ACB5D8A4A8A57C794A1082F3D</mso:ContentTypeId>
<mso:SeoBrowserTitle msdt:dt="string">Servicio Nacional de Aprendizaje SENA</mso:SeoBrowserTitle>
<mso:SeoKeywords msdt:dt="string">SENA, Formación, Empleo, Emprendimiento, Trabajo</mso:SeoKeywords>
<mso:RobotsNoIndex msdt:dt="string">0</mso:RobotsNoIndex>
<mso:SeoMetaDescription msdt:dt="string">Página oficial de la Entidad de formación para el trabajo del Estado colombiano. Información y oportunidades de formación, empleo y emprendimiento.</mso:SeoMetaDescription>
</mso:CustomDocumentProperties>
</xml></SharePoint:CTFieldRefs><![endif]-->
<title>Servicio Nacional de Aprendizaje SENA</title></head>